import numpy as np
import os
import matplotlib.pyplot as plt
import keras
from keras.preprocessing import image
from keras.applications.mobilenet import preprocess_input
from keras.backend import clear_session
import tensorflow as tf

global graph
graph = tf.get_default_graph()

def show_prediction(model):
    route = 'predict'
    path = os.getcwd()+'/'+route+'/'
    files= os.listdir(path)

    #keras.backend.clear_session()

    #model = load_model('./save.h5')
    classes = ['Tabby','Persian','Siamese','Egyptian']
    cat_identifier = {'category':'cat', 'certainity':'rate'}

    

    for file in files:
        img = image.load_img(path+file, target_size=(250,250))
        img_array = image.img_to_array(img)
        img_array_expanded_dims = np.expand_dims(img_array, axis=0)
        processed_img = preprocess_input(img_array_expanded_dims)
        
        with graph.as_default():
            prediction = model.predict(processed_img)

        #print("Tabby Cat Certainty:  %.2f%%" % (prediction[0,0]*100))
        #print("Persian Certainty:  %.2f%%" % (prediction[0,1]*100))
        #print("Siamese Certainty:  %.2f%%" % (prediction[0,2]*100))
        prediction_index = prediction.argmax()
        prediction = np.max(prediction)

        #print("The predicted animal is a " + classes[int(prediction_index)] + " with %.2f%% certainity \n\n" %(prediction*100))
        #plt.figure()
        #plt.imshow(img)
        #plt.show()

        cat_identifier['category'] = str(classes[int(prediction_index)])
        cat_identifier['certainity'] = str(prediction*100)
        break
    
    return cat_identifier

#show_prediction()


