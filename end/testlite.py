import matplotlib.pyplot as plt
import numpy as np
from keras.models import load_model
from keras.applications.inception_v3 import preprocess_input
from keras.preprocessing import image
import numpy as np
import os
import matplotlib.pyplot as plt
from keras.preprocessing import image
from keras.models import load_model

predict_data_directory = './predict'

quan_model = interpreter_wrapper.Interpreter(model_path='./tflitefile.tflite')

quan_model.allocate_tensors()


quan_input_details = quan_model.get_input_details()
quan_output_details = quan_model.get_output_details()

print('Models Loaded',quan_input_details,quan_output_details)

route = './predict'
path = os.getcwd()+'/'+route+'/'
files= os.listdir(path) 
classes = ['Tabby','Persian','Siamese','Egyptian']

for file in files:
    img = image.load_img(path+file, target_size=(299,299,3))
    img_array = image.img_to_array(img)
    img_array_expanded_dims = np.expand_dims(img_array, axis=0)
    processed_img = preprocess_input(img_array_expanded_dims)
    
    print(quan_input_details[0]['index'])
    quan_model.set_tensor(quan_input_details[0]['index'], processed_img)
    quan_model.invoke()
    quan_output = quan_model.get_tensor(quan_output_details[0]['index'])

    print(str(quan_output))
    print("Tabby Cat Certainty:  %.2f%%" % (quan_output[0,0]*100))
    print("Persian Certainty:  %.2f%%" % (quan_output[0,1]*100))
    print("Siamese Certainty:  %.2f%%" % (quan_output[0,2]*100))
    print("Egyptian Certainty:  %.2f%%" % (quan_output[0,3]*100))
   
